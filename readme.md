# Backup, Check and Restore Repository SVN and GIT
required: OS Linux with git, subversion, docker, gzip, sha256sum
template: https://www.uxora.com/unix/shell-script/18-shell-script-template

## Usage SVN Backup-Check-Restore:
Backup
```
svn-backup.sh -s /path/to/svn/repos/sample-repo -d /path/to/destination/svnbackup -o DEFAULT backup
```
Ckeck data integrity
```
svn-backup.sh -s /path/to/destination/svnbackup.dump.gz.sha256 -o DEFAULT check
```
Restore Dump in docker Container (coming soon)
```
svn-backup.sh -s /path/to/destination/svnbackup.dump.gz -o DEFAULT restore
```
## Usage GIT Backup-Check-Restore:
Backup
```
git-backup.sh -s /path/to/git/repos/sample-repo -d /path/to/destination/gitbackup -o DEFAULT backup
```
Ckeck data integrity
```
git-backup.sh -s /path/to/destination/gitbackup.tar.gz.sha256 -o DEFAULT check
```
Restore Dump in docker Container (coming soon)
```
git-backup.sh -s /path/to/destination/gitbackup.tar.gz -o DEFAULT restore
```